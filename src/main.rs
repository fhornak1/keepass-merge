mod cmdargs;
mod datastructs;
mod db;
mod merge;
mod opts;
mod tui;

use anyhow::Result;

use console::Term;
use dialoguer::theme::ColorfulTheme;

use crate::db::prelude::*;
use crate::merge::prelude::*;

fn main() -> Result<()> {
    let args = cmdargs::parse();
    println!("Args: {:?}", args);
    let term = Term::buffered_stderr();
    let theme = ColorfulTheme::default();
    let options = opts::from_args(args, &term, &theme)?;
    println!("Options: {:?}", options);
    let ref_db = load(&options.reference)?;
    let mut kdb = KeePassDB::open(&options.reference)?;
    let upd = KeePassDB::open(&options.update)?;

    let entries = resolve_modifications(kdb.borrow_entries(), upd.borrow_entries());

    kdb.upsert_all(entries);

    for elm in ref_db {
        println!("\n{:?}", elm.1)
    }
    //
    // // db::load(args)
    //
    // let items = &[
    //     "Ice Cream",
    //     "Vanilla Cupcake",
    //     "Chocolate Muffin",
    //     "A Pile of sweet, sweet mustard",
    // ];
    //
    // println!("All the following controls are run in a buffered terminal");
    // Confirm::with_theme(&theme)
    //     .with_prompt("Do you want to continue?")
    //     .interact_on(&term)?;
    //
    // let _: String = Input::with_theme(&theme)
    //     .with_prompt("Your name")
    //     .interact_on(&term)?;
    //
    // Select::with_theme(&theme)
    //     .with_prompt("Pick an item")
    //     .items(items)
    //     .interact_on(&term)?;
    //
    // MultiSelect::with_theme(&theme)
    //     .with_prompt("Pick some items")
    //     .items(items)
    //     .interact_on(&term)?;
    //
    // Sort::with_theme(&theme)
    //     .with_prompt("Order these items")
    //     .items(items)
    //     .interact_on(&term)?;
    //
    // let password = Password::with_theme(&ColorfulTheme::default())
    //     .with_prompt("Password")
    //     .with_confirmation("Repeat password", "Error: the passwords don't match.")
    //     .interact_on(&term)?;
    Ok(())
}
