use crate::datastructs::rclist::List;
use crate::db::prelude::{DBEntryRefs, GroupNode};
use crate::merge::diff::{Change, New};
use keepass::db::Entry;

pub fn resolve_modifications(
    reference: DBEntryRefs<'_>,
    update: DBEntryRefs<'_>,
) -> Vec<Modification> {
    todo!()
}

fn update(entry_update: Change) -> Modification {
    todo!()
}

fn insert(entry_new: New) -> Modification {
    Modification::Insert(Insert {
        path: entry_new.groups,
        entry: entry_new.entry,
    })
}

pub enum Modification {
    Insert(Insert),
    Update(Update),
}

pub struct Insert {
    path: List<GroupNode>,
    entry: Entry,
}

pub struct Update {
    path: List<GroupNode>,
    entry: Entry,
}
