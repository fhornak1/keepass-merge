use anyhow::{Context, Result};
use keepass::db::{Entry, Group, Node, NodeRef, NodeRefMut};
use keepass::{Database, DatabaseKey};
use std::collections::HashMap;

use std::convert::From;

use std::fs::File;
use std::path::PathBuf;

use crate::datastructs::rclist::prelude::*;
use crate::db::iter::GroupedEntryIter;
use crate::db::model::{DatabaseEntries, GroupNode, GroupedEntry};
use crate::db::prelude::DBEntryRefs;
use crate::merge::prelude::{Insert, Modification, Update};
use crate::opts::{DBAccess, WithKeyfile};

pub fn load(access: &DBAccess) -> Result<DatabaseEntries> {
    open(access).map(|db| entries(&db))
}

pub fn open(access: &DBAccess) -> Result<Database> {
    match access {
        DBAccess::PasswordOnly(pass) => _internal_open(
            &pass.path,
            DatabaseKey::with_password(pass.password.as_ref()),
        ),
        DBAccess::WithKeyfile(key) => _open_with_keyfile(key),
    }
}

fn _open_with_keyfile(wk: &WithKeyfile) -> Result<Database> {
    let keyfile = &mut File::open(&wk.keyfile_path).with_context(|| {
        format!(
            "Failed to open keyfile: {}",
            wk.keyfile_path.to_string_lossy()
        )
    })?;
    _internal_open(
        &wk.path,
        DatabaseKey::with_password_and_keyfile(&wk.password, keyfile),
    )
}

fn _internal_open(path: &PathBuf, key: DatabaseKey) -> Result<Database> {
    let dbfile = &mut File::open(&path)
        .with_context(|| format!("failed to open database file: {}", path.to_string_lossy()))?;
    Database::open(dbfile, key)
        .with_context(|| format!("Failed to open database: {}", path.to_string_lossy()))
}

pub struct KeePassDB {
    db: Database,
    cache: HashMap<Vec<GroupNode>, Vec<String>>,
}

impl KeePassDB {
    pub fn open(access: &DBAccess) -> Result<Self> {
        open(access).map(|db| Self {
            db,
            cache: HashMap::new(),
        })
    }

    pub fn database(&self) -> &Database {
        &self.db
    }

    pub fn save(&self, dest: &DBAccess) -> Result<()> {
        todo!()
    }

    pub fn borrow_entries(&self) -> DBEntryRefs {
        GroupedEntryIter::new(List::empty(), NodeRef::Group(&self.db.root))
            .map(|ger| (ger.uuid(), ger))
            .collect()
    }

    pub fn entries(&self) -> DatabaseEntries {
        entries(&self.db)
    }

    pub fn upsert_all(&mut self, mods: Vec<Modification>) {
        todo!()
    }

    pub fn upsert(&mut self, modification: Modification) {
        match modification {
            Modification::Update(update) => self.update(update),
            Modification::Insert(insert) => self.insert(insert),
        }
    }

    pub fn insert(&mut self, insert: Insert) {
        todo!()
    }

    pub fn update(&mut self, update: Update) {
        todo!()
    }

    fn update_entry_by_group_id(
        &mut self,
        groups: &List<GroupNode>,
        entry: Entry,
    ) -> Option<Entry> {
        self.find_group_by_id_mut(&groups)
            .and_then(|gr| gr.update_entry_by_id(entry))
    }

    fn find_group_by_id_mut<'b, 'a: 'b>(
        &'a mut self,
        groups: &List<GroupNode>,
    ) -> Option<&'b mut Group> {
        groups.into_iter().fold(Some(&mut self.db.root), |acc, gn| {
            acc.and_then(|gr| gr.find_group_by_id_mut(&gn.uuid))
        })
    }

    fn find_group_by_name_mut<'b, 'a: 'b>(
        &'a mut self,
        groups: &List<GroupNode>,
    ) -> Option<&'b mut Group> {
        groups.into_iter().fold(Some(&mut self.db.root), |acc, gn| {
            acc.and_then(|gr| gr.find_group_by_name_mut(&gn.name))
        })
    }

    fn insert_entry_by_path(&mut self, groups: &List<GroupNode>, entry: Entry) {
        let path = self.find_path(groups);
        let group = self.get_or_create_group_mut(path);
        group.children.push(Node::Entry(entry));
    }

    fn find_path(&self, groups: &List<GroupNode>) -> List<PathMarker> {
        groups
            .into_iter()
            .fold(
                (List::empty(), Some(&self.db.root)),
                |(lst, mgr), gn| match mgr {
                    None => (lst.prepend(PathMarker::NewGroup(gn.name.to_string())), None),
                    Some(gr) => {
                        let (pm, maybe_group) = gr
                            .group_marker(gn)
                            .map(|(pm, gr)| (pm, Some(gr)))
                            .unwrap_or((PathMarker::NewGroup(gn.name.clone()), None));
                        (lst.prepend(pm), maybe_group)
                    }
                },
            )
            .0
            .rev()
    }

    fn get_or_create_group_mut<'b, 'a: 'b>(&'a mut self, pm: List<PathMarker>) -> &'b mut Group {
        todo!()
    }
}

fn entries(db: &Database) -> DatabaseEntries {
    GroupedEntryIter::new(List::empty(), NodeRef::Group(&db.root))
        .map(|ger| (ger.uuid(), GroupedEntry::from(ger)))
        .collect()
}

trait GroupOps {
    fn find_group_by_id<'b, 'a: 'b>(&'a self, uuid: &str) -> Option<&'b Group> {
        self.find_by(|nr| match nr {
            NodeRef::Group(gr) if &gr.uuid == uuid => Some(gr),
            _ => None,
        })
    }

    fn find_group_by_name<'b, 'a: 'b>(&'a self, name: &str) -> Option<&'b Group> {
        self.find_by(|nr| match nr {
            NodeRef::Group(gr) if &gr.name == name => Some(gr),
            _ => None,
        })
    }

    fn find_by<'a, B: Sized, F: Fn(NodeRef<'a>) -> Option<B>>(&'a self, pr: F) -> Option<B>;

    fn group_marker<'b, 'a: 'b>(&'a self, node: &GroupNode) -> Option<(PathMarker, &'b Group)> {
        self.find_group_by_id(&node.uuid)
            .map(|gr| (PathMarker::Id(node.uuid.clone()), gr))
            .or_else(|| {
                self.find_group_by_name(&node.name)
                    .map(|gr| (PathMarker::Id(gr.uuid.clone()), gr))
            })
    }

    fn find_by_mut<'a, B: Sized, F: FnMut(NodeRefMut<'a>) -> Option<B>>(
        &'a mut self,
        pr: F,
    ) -> Option<B>;

    fn find_group_by_id_mut<'b, 'a: 'b>(&'a mut self, uuid: &str) -> Option<&'b mut Group> {
        self.find_by_mut(|nr| match nr {
            NodeRefMut::Group(gr) if &gr.uuid == uuid => Some(gr),
            _ => None,
        })
    }

    fn find_group_by_name_mut<'b, 'a: 'b>(&'a mut self, name: &str) -> Option<&'b mut Group> {
        self.find_by_mut(|nr| match nr {
            NodeRefMut::Group(gr) if &gr.name == name => Some(gr),
            _ => None,
        })
    }

    fn find_entry_by_id_mut<'b, 'a: 'b>(&'a mut self, uuid: &str) -> Option<&'b mut Entry> {
        self.find_by_mut(|nr| match nr {
            NodeRefMut::Entry(en) if &en.uuid == uuid => Some(en),
            _ => None,
        })
    }

    fn update_entry_by_id(&mut self, entry: Entry) -> Option<Entry> {
        self.find_entry_by_id_mut(entry.get_uuid())
            .map(|e| std::mem::replace(e, entry))
    }
}

impl GroupOps for Group {
    fn find_by<'a, B: Sized, F: Fn(NodeRef<'a>) -> Option<B>>(&'a self, pr: F) -> Option<B> {
        self.children.iter().flat_map(|e| pr(e.as_ref())).nth(0)
    }

    fn find_by_mut<'a, B: Sized, F: FnMut(NodeRefMut<'a>) -> Option<B>>(
        &'a mut self,
        mut pr: F,
    ) -> Option<B> {
        self.children.iter_mut().flat_map(|e| pr(e.as_mut())).nth(0)
    }
}

#[derive(Clone)]
enum PathMarker {
    Id(String),
    NewGroup(String),
}
