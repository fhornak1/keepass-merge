use std::fmt::{Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::rc::Rc;

pub trait ListOps<T> {
    fn prepend(&self, element: T) -> Self;

    fn head(&self) -> Option<&T>;

    fn tail(&self) -> Self;

    fn split(&self) -> (Option<&T>, Self);
}

#[derive(Clone)]
pub struct List<T>
where
    T: Sized,
{
    internal: Rc<ListElms<T>>,
    size: usize,
}

enum ListElms<T: Sized> {
    Nil,
    Cons(T, Rc<ListElms<T>>),
}

impl<T> List<T> {
    pub fn len(&self) -> usize {
        self.size
    }

    pub fn empty() -> Self {
        List {
            internal: Rc::new(ListElms::Nil),
            size: 0,
        }
    }

    pub fn singleton(elm: T) -> Self {
        Self::empty().prepend(elm)
    }

    pub fn pair(elm1: T, elm2: T) -> Self {
        Self::singleton(elm2).prepend(elm1)
    }

    pub fn rev(&self) -> List<T>
    where
        T: Clone,
    {
        self.into_iter()
            .fold(List::empty(), |acc, elm| acc.prepend(elm.to_owned()))
    }
}

impl<'a, T> IntoIterator for &'a List<T> {
    type Item = &'a T;
    type IntoIter = RefListIter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        RefListIter {
            it: self.internal.as_ref(),
            size: self.size,
        }
    }
}

impl<T> PartialEq for List<T>
where
    T: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        let mut self_it = self.into_iter();
        let mut other_it = other.into_iter();

        loop {
            match (self_it.next(), other_it.next()) {
                (None, None) => return true,
                (Some(s), Some(o)) if s.eq(o) => {}
                _ => return false,
            }
        }
    }
}
impl<T: PartialEq> Eq for List<T> {}

impl<T: Hash> Hash for List<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for elm in self {
            elm.hash(state)
        }
    }
}

impl<T> ListOps<T> for List<T>
where
    T: Sized,
{
    fn prepend(&self, element: T) -> Self {
        List {
            internal: Rc::new(ListElms::Cons(element, self.internal.clone())),
            size: self.size + 1,
        }
    }

    fn head(&self) -> Option<&T> {
        self.split().0
    }

    fn tail(&self) -> Self {
        self.split().1
    }

    fn split(&self) -> (Option<&T>, Self) {
        match self.internal.as_ref() {
            ListElms::Nil => (None, List::empty()),
            ListElms::Cons(hd, tl) => (
                Some(hd),
                List {
                    internal: tl.clone(),
                    size: self.size - 1,
                },
            ),
        }
    }
}

impl<T: Display> Display for List<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let (hd, tl) = self.split();
        match hd {
            None => {}
            Some(elm) => {
                write!(f, "{}", elm)?;
                for other in &tl {
                    write!(f, " :: {}", other)?;
                }
            }
        }
        Ok(())
    }
}

impl<T: Debug> Debug for List<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let (hd, tl) = self.split();
        write!(f, "List [")?;
        match hd {
            None => {}
            Some(elm) => {
                write!(f, "{:?}", elm)?;
                for other in &tl {
                    write!(f, " :: {:?}", other)?;
                }
            }
        }
        write!(f, "]")
    }
}

impl<T> FromIterator<T> for List<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        iter.into_iter()
            .fold(List::empty(), |acc: List<T>, elm: T| acc.prepend(elm))
    }
}

pub struct RefListIter<'a, T> {
    it: &'a ListElms<T>,
    size: usize,
}

impl<'a, T> Iterator for RefListIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        match self.it {
            ListElms::Nil => None,
            ListElms::Cons(hd, tl) => {
                self.it = tl.as_ref();
                self.size -= 1;
                Some(hd)
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.size, Some(self.size))
    }
}

impl<'a, T> ExactSizeIterator for RefListIter<'a, T> {
    fn len(&self) -> usize {
        self.size
    }
}

pub mod prelude {
    pub use crate::datastructs::rclist::{List, ListOps};
}
