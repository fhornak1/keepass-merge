use crate::cmdargs::{CmdArgs, FastForwardMode, Interactive};
use anyhow::{Context, Result};
use console::{style, Term};
use dialoguer::theme::Theme;
use dialoguer::Password;
use std::convert::From;
use std::fmt::{Debug, Formatter};
use std::path::PathBuf;

pub(crate) fn from_args(cmd: CmdArgs, term: &Term, theme: &dyn Theme) -> Result<Opts> {
    let password = if cmd.shared_password {
        Some(
            Password::with_theme(theme)
                .with_prompt(format!(
                    "Password to access {} databases:",
                    style("all").green()
                ))
                .report(true)
                .interact_on(term)
                .with_context(|| "Writing into terminal failed")?,
        )
    } else {
        None
    };
    let reference = mk_dbaccess(
        "reference",
        &cmd.into,
        cmd.reference_keyfile(),
        password.as_ref().or_else(|| cmd.reference_password()),
        term,
        theme,
    )?;
    let update = mk_dbaccess(
        "update",
        &cmd.from,
        cmd.update_keyfile(),
        password.as_ref().or_else(|| cmd.update_password()),
        term,
        theme,
    )?;
    let output = match (cmd.dry_run, cmd.in_place, cmd.backup) {
        (true, _, _) => OutputOpts::OnlyPrint,
        (_, true, false) => OutputOpts::InPlace(reference.clone()),
        (_, true, true) => OutputOpts::InPlaceWithBackup(reference.clone()),
        (_, false, _) => {
            let output = cmd
                .output
                .as_ref()
                .with_context(|| "Non --in-place update missing output path".to_owned())?;
            let access = mk_dbaccess(
                "output",
                output,
                cmd.output_keyfile(),
                password.as_ref().or_else(|| cmd.output_password()),
                term,
                theme,
            )?;
            if cmd.backup && cmd.overwrite {
                OutputOpts::OutputBackupAndOverwrite(access)
            } else if cmd.overwrite {
                OutputOpts::OutputOverwrite(access)
            } else {
                OutputOpts::Output(access)
            }
        }
    };
    Ok(Opts {
        reference,
        update,
        output,
        fast_forward: cmd.fast_forward,
        tty: cmd.tty,
        show_password: cmd.show_password,
        verbose: cmd.verbose,
    })
}

fn mk_dbaccess(
    dbname: &str,
    dbpath: &PathBuf,
    keyfile: Option<&PathBuf>,
    args_passwd: Option<&String>,
    term: &Term,
    theme: &dyn Theme,
) -> Result<DBAccess> {
    let path = dbpath.to_owned();
    ask_password(args_passwd, term, theme, dbname, dbpath).map(|password| match keyfile {
        None => DBAccess::PasswordOnly(PasswordOnly { path, password }),
        Some(kf) => DBAccess::WithKeyfile(WithKeyfile {
            path,
            password,
            keyfile_path: kf.to_owned(),
        }),
    })
}

fn ask_password(
    passwd: Option<&String>,
    term: &Term,
    theme: &dyn Theme,
    dbname: &str,
    pb: &PathBuf,
) -> Result<String> {
    match passwd {
        None => Password::with_theme(theme)
            .with_prompt(format!(
                "Password for {} database ({}):",
                style(dbname).green(),
                style(pb.to_string_lossy()).dim()
            ))
            .report(true)
            .interact_on(term)
            .with_context(|| "Writing into terminal failed"),
        Some(pwd) => Ok(pwd.to_string()),
    }
}

#[derive(Debug)]
pub struct Opts {
    pub reference: DBAccess,
    pub update: DBAccess,
    pub output: OutputOpts,
    pub fast_forward: FastForwardMode,
    pub tty: Interactive,
    pub show_password: bool,
    pub verbose: bool,
}

#[derive(Debug)]
pub enum OutputOpts {
    OnlyPrint,
    InPlace(DBAccess),
    InPlaceWithBackup(DBAccess),
    Output(DBAccess),
    OutputOverwrite(DBAccess),
    OutputBackupAndOverwrite(DBAccess),
}

#[derive(Debug, Clone)]
pub enum DBAccess {
    PasswordOnly(PasswordOnly),
    WithKeyfile(WithKeyfile),
}

impl DBAccess {
    pub(crate) fn with_keyfile(path: PathBuf, password: String, keyfile_path: PathBuf) -> Self {
        DBAccess::WithKeyfile(WithKeyfile {
            path,
            password,
            keyfile_path,
        })
    }

    pub(crate) fn only_password(path: PathBuf, password: String) -> Self {
        DBAccess::PasswordOnly(PasswordOnly { path, password })
    }
}

#[derive(Clone)]
pub struct PasswordOnly {
    pub path: PathBuf,
    pub password: String,
}

impl Debug for PasswordOnly {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "PasswordOnly {{ path: {}, password: {} }}",
            self.path.to_string_lossy(),
            self.password.replace(|_| true, "*")
        )
    }
}

#[derive(Clone)]
pub struct WithKeyfile {
    pub path: PathBuf,
    pub password: String,
    pub keyfile_path: PathBuf,
}

impl Debug for WithKeyfile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "WithKeyfile {{ path: {}, password: {}, keyfile_path: {} }}",
            self.path.to_string_lossy(),
            self.password.replace(|_| true, "*"),
            self.keyfile_path.to_string_lossy()
        )
    }
}
