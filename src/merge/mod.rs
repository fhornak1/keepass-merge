pub mod diff;
pub mod resoler;

pub mod prelude {
    pub use crate::merge::resoler::{resolve_modifications, Insert, Modification, Update};
}
