use crate::datastructs::rclist::List;
use crate::db::prelude::{DBEntryRefs, GroupNode, GroupedEntryRef, TimedEntry};
use keepass::db::Entry;

pub(crate) fn find_missing(original: &DBEntryRefs, update: &DBEntryRefs) -> Vec<Diff> {
    update
        .into_iter()
        .flat_map(|(key, entry)| {
            original
                .get(key)
                .is_none()
                .then(|| Diff::insert(entry.groups(), entry.entry()))
        })
        .collect()
}

pub(crate) fn find_modified(orig: &DBEntryRefs, update: &DBEntryRefs) -> Vec<Diff> {
    orig.into_iter()
        .flat_map(|(key, entry)| {
            update.get(key).and_then(|ue| {
                has_changed(&entry, &ue)
                    .then(|| Diff::update(entry.groups(), entry.entry(), ue.entry()))
            })
        })
        .collect()
}

fn has_changed(reference: &GroupedEntryRef, update: &GroupedEntryRef) -> bool {
    reference.last_modification_time() < update.last_modification_time()
        && reference.borrow_entry().fields != update.borrow_entry().fields
        && reference.borrow_entry().custom_data != update.borrow_entry().custom_data
}

pub(crate) enum Diff {
    New(New),
    Change(Change),
}

impl Diff {
    fn insert(groups: List<GroupNode>, entry: Entry) -> Diff {
        Self::New(New { groups, entry })
    }

    fn update(groups: List<GroupNode>, reference: Entry, update: Entry) -> Diff {
        Self::Change(Change {
            groups,
            reference,
            update,
        })
    }
}

pub(crate) struct New {
    pub(crate) groups: List<GroupNode>,
    pub(crate) entry: Entry,
}

pub(crate) struct Change {
    pub(crate) groups: List<GroupNode>,
    pub(crate) reference: Entry,
    pub(crate) update: Entry,
}
