mod iter;
mod model;
mod open;

pub mod prelude {
    pub use crate::db::model::{
        DBEntryRefs, DatabaseEntries, GroupNode, GroupedEntry, GroupedEntryRef, TimedEntry,
    };
    pub use crate::db::open::{load, open, KeePassDB};
}
