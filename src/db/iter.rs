use keepass::{self, db::NodeRef};
use std::collections::VecDeque;

use crate::datastructs::rclist::prelude::*;
use crate::db::model::{GroupNode, GroupedEntryRef};

struct GroupedElement<'a> {
    path: List<GroupNode>,
    entry: NodeRef<'a>,
}

impl<'a> GroupedElement<'a> {
    pub(crate) fn new(path: List<GroupNode>, entry: NodeRef<'a>) -> Self {
        GroupedElement { path, entry }
    }
}

pub(crate) struct GroupedEntryIter<'a> {
    queue: VecDeque<GroupedElement<'a>>,
}

impl<'a> GroupedEntryIter<'a> {
    pub fn from_root(root: NodeRef<'a>) -> Self {
        Self::new(List::empty(), root)
    }

    pub fn new(path: List<GroupNode>, entry: NodeRef<'a>) -> Self {
        let mut queue: VecDeque<GroupedElement<'a>> = VecDeque::with_capacity(10);
        queue.push_front(GroupedElement { path, entry });
        Self { queue }
    }
}

impl<'a> Iterator for GroupedEntryIter<'a> {
    type Item = GroupedEntryRef<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.queue.pop_front().and_then(|head| match head.entry {
            NodeRef::Group(gr) => {
                let prep = head.path.prepend(gr.into());
                self.queue.extend(
                    gr.children
                        .iter()
                        .map(|n| GroupedElement::new(prep.clone(), n.into())),
                );
                self.next()
            }
            NodeRef::Entry(entry) => Some(GroupedEntryRef::new(head.path.clone(), entry)),
        })
    }
}
