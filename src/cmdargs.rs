use clap::error::ErrorKind;
use clap::{ArgGroup, CommandFactory, Parser, ValueEnum};

use std::borrow::ToOwned;
use std::fmt::Debug;
use std::path::PathBuf;

pub fn parse() -> CmdArgs {
    let cli = CmdArgs::parse();

    check_disabled_tty(&cli);

    cli
}

fn check_disabled_tty(cli: &CmdArgs) {
    if cli.tty == Interactive::Disabled {
        let from_pass = cli.from_password.as_ref().or_else(|| cli.password.as_ref());
        let to_pass = cli.into_password.as_ref().or_else(|| cli.password.as_ref());
        let out_pass = match cli.output.as_ref() {
            Some(_) => cli
                .output_password
                .as_deref()
                .or_else(|| cli.password.as_deref()),
            None => Some("-"),
        };
        match (from_pass, to_pass, out_pass) {
            (Some(_), Some(_), Some(_)) => {}
            _ => {
                let mut cmd = CmdArgs::command();
                cmd.error(
                    ErrorKind::ValueValidation,
                    "When --tty disabled, passwords must be specified",
                )
                .exit();
            }
        }
        if cli.fast_forward == FastForwardMode::Disabled {
            let mut cmd = CmdArgs::command();
            cmd.error(
                ErrorKind::ValueValidation,
                "Both --fast-forward and --tty can not be both disabled at the same time",
            )
            .exit();
        }
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
#[command(group(ArgGroup::new("passwds").args(["password", "from_password", "into_password", "output_password"]).multiple(true).conflicts_with("shared_password")))]
#[command(group(ArgGroup::new("out").args(["output_password", "output_keyfile", "output"]).multiple(true).requires("output").conflicts_with("in_place")))]
pub struct CmdArgs {
    /// Path to database with updates
    pub from: PathBuf,
    /// Path to reference database
    pub into: PathBuf,
    /// Path to output database
    #[arg(short, long)]
    pub output: Option<PathBuf>,

    /// Password for database with updates
    #[arg(long)]
    pub from_password: Option<String>,

    /// Password for reference database
    #[arg(long)]
    pub into_password: Option<String>,

    /// Password for output database
    #[arg(long)]
    pub output_password: Option<String>,

    /// Password for all databases
    #[arg(short, long)]
    pub password: Option<String>,

    /// All databases use the same password
    #[arg(short, long)]
    pub shared_password: bool,

    /// Path to keyfile of database with updates
    #[arg(long)]
    pub from_keyfile: Option<PathBuf>,

    /// Path to keyfile of reference database
    #[arg(long)]
    pub into_keyfile: Option<PathBuf>,

    /// Path to keyfile of output database
    #[arg(long)]
    pub output_keyfile: Option<PathBuf>,

    /// Path to keyfile for all databases
    #[arg(short, long)]
    pub keyfile: Option<PathBuf>,

    /// Changes would be written to reference database (NOT RECOMMENDED)
    #[arg(long)]
    pub in_place: bool,

    /// Overwrite output database
    #[arg(long)]
    pub overwrite: bool,

    /// Set fast forward mode, to scan through non conflicting changes.
    #[arg(long, value_enum, default_value_t = FastForwardMode::Disabled)]
    pub fast_forward: FastForwardMode,

    /// Set interactive mode.
    #[arg(long, value_enum, default_value_t = Interactive::Complete)]
    pub tty: Interactive,

    /// Only prints changes performed over database
    #[arg(long)]
    pub dry_run: bool,

    /// Display passwords (NOT RECOMMENDED)
    #[arg(long)]
    pub show_password: bool,

    /// Backup output or reference database
    #[arg(short, long)]
    pub backup: bool,

    /// Increase verbosity level
    #[arg(short, long)]
    pub verbose: bool,
}

impl CmdArgs {
    pub fn reference_password(&self) -> Option<&String> {
        self.into_password
            .as_ref()
            .or_else(|| self.password.as_ref())
    }

    pub fn update_password(&self) -> Option<&String> {
        self.from_password
            .as_ref()
            .or_else(|| self.password.as_ref())
    }

    pub fn output_password(&self) -> Option<&String> {
        self.output_password
            .as_ref()
            .or_else(|| self.password.as_ref())
    }

    pub fn reference_keyfile(&self) -> Option<&PathBuf> {
        self.into_keyfile.as_ref().or_else(|| self.keyfile.as_ref())
    }

    pub fn update_keyfile(&self) -> Option<&PathBuf> {
        self.from_keyfile.as_ref().or_else(|| self.keyfile.as_ref())
    }

    pub fn output_keyfile(&self) -> Option<&PathBuf> {
        self.output_keyfile
            .as_ref()
            .or_else(|| self.keyfile.as_ref())
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum FastForwardMode {
    /// Disable fast-forward
    Disabled,
    /// Insert new entries automatically
    OnlyInsert,
    /// On conflicting update use timestamp as reference
    MergeByTimestamp,
    /// This option will reinsert conflicting entries with new uuid
    ConflictsAsNewEntry,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum Interactive {
    /// Disabled tty
    Disabled,
    /// Minimal interactive mode
    Minimal,
    /// Complete interactive mode
    Complete,
}
