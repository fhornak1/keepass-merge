use crate::datastructs::rclist::prelude::*;
use chrono::NaiveDateTime;
use keepass::db::{Entry, Group};
use std::collections::HashMap;

pub trait TimedEntry {
    fn last_modification_time(&self) -> Option<&NaiveDateTime> {
        self.time_for("LastModificationTime")
    }

    fn last_access_time(&self) -> Option<&NaiveDateTime> {
        self.time_for("LastAccessTime")
    }

    fn location_changed(&self) -> Option<&NaiveDateTime> {
        self.time_for("LocationChanged")
    }

    fn creation_time(&self) -> Option<&NaiveDateTime> {
        self.time_for("CreationTime")
    }

    fn expiry_time(&self) -> Option<&NaiveDateTime> {
        self.time_for("ExpiryTime")
    }

    fn time_for(&self, key: &str) -> Option<&NaiveDateTime>;
}

impl TimedEntry for Entry {
    fn time_for(&self, key: &str) -> Option<&NaiveDateTime> {
        self.get_time(key)
    }
}

#[derive(Debug, Clone)]
pub struct GroupNode {
    pub uuid: String,
    pub name: String,
}

impl PartialEq for GroupNode {
    fn eq(&self, other: &Self) -> bool {
        self.uuid == other.uuid
    }
}

impl Eq for GroupNode {}

impl From<&Group> for GroupNode {
    fn from(value: &Group) -> Self {
        GroupNode {
            uuid: value.uuid.clone(),
            name: value.name.clone(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct GroupedEntry {
    groups: List<GroupNode>,
    entry: Entry,
}

impl GroupedEntry {
    pub fn new(groups: List<GroupNode>, entry: Entry) -> Self {
        Self { groups, entry }
    }

    pub fn groups(&self) -> List<GroupNode> {
        self.groups.clone()
    }

    pub fn borrow_groups(&self) -> &List<GroupNode> {
        &self.groups
    }

    pub fn borrow_entry(&self) -> &Entry {
        &self.entry
    }

    pub fn uuid(&self) -> String {
        self.entry.get_uuid().to_owned()
    }

    pub fn borrow_uuid(&self) -> &str {
        self.entry.get_uuid()
    }

    pub fn unapply(self) -> (List<GroupNode>, Entry) {
        (self.groups, self.entry)
    }
}

impl TimedEntry for GroupedEntry {
    fn time_for(&self, key: &str) -> Option<&NaiveDateTime> {
        self.entry.get_time(key)
    }
}

impl<'a> From<GroupedEntryRef<'a>> for GroupedEntry {
    fn from(value: GroupedEntryRef<'a>) -> Self {
        Self {
            groups: value.groups,
            entry: value.entry.clone(),
        }
    }
}

impl<'a> From<&GroupedEntryRef<'a>> for GroupedEntry {
    fn from(value: &GroupedEntryRef<'a>) -> Self {
        Self {
            groups: value.groups.clone(),
            entry: value.entry.clone(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct GroupedEntryRef<'a> {
    groups: List<GroupNode>,
    entry: &'a Entry,
}

impl<'a> GroupedEntryRef<'a> {
    pub fn new(groups: List<GroupNode>, entry: &'a Entry) -> Self {
        Self { groups, entry }
    }

    pub fn borrow_groups(&self) -> &List<GroupNode> {
        &self.groups
    }

    pub fn groups(&self) -> List<GroupNode> {
        self.groups.clone()
    }

    pub fn borrow_entry(&self) -> &Entry {
        &self.entry
    }

    pub fn entry(&self) -> Entry {
        self.entry.clone()
    }

    pub fn uuid(&self) -> String {
        self.entry.get_uuid().to_owned()
    }

    pub fn borrow_uuid(&self) -> &str {
        self.entry.get_uuid()
    }
}

impl PartialEq for GroupedEntryRef<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.entry.get_uuid() == other.entry.get_uuid()
            && self.entry.last_modification_time() == other.entry.last_modification_time()
            && self.entry.location_changed() == other.entry.location_changed()
            && self.groups == other.groups
            && self.entry.fields == other.entry.fields
            && self.entry.custom_data == other.entry.custom_data
    }
}

impl Eq for GroupedEntryRef<'_> {}

impl TimedEntry for GroupedEntryRef<'_> {
    fn time_for(&self, key: &str) -> Option<&NaiveDateTime> {
        self.entry.get_time(key)
    }
}

pub type DatabaseEntries = HashMap<String, GroupedEntry>;

pub type DBEntryRefs<'a> = HashMap<String, GroupedEntryRef<'a>>;
